<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    protected $table = "pemesanan";
    protected $fillable = ["pelanggan_id", 'kamar_id', 'lama_menginap', 'total_harga'];
    public $timestamps = true;

    public function pembayaran(){
        return $this->hasOne('App\Pembayaran');
    }

    public function pelanggan(){
        return $this->belongsTo('App\pelanggan');
    }

    public function kamar(){
        return $this->belongsTo('App\Kamar');
    }
}
