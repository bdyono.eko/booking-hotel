<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipe extends Model
{
    protected $table = "tipe";
    protected $fillable = ["nama_tipe", "gambar", "harga", "deskripsi", "fasilitas", "created_at", "updated_at"];
    public $timestamps = true;

    public function kamar(){
        return $this->hasMany('App\Kamar');
    }
}
