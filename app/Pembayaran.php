<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = "pembayaran";
    protected $fillable = ["pemesanan_id", "user_id", "bukti_bayar", "konfirmasi"];
    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function pemesanan(){
        return $this->belongsTo('App\Pemesanan');
    }
}
