<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kamar;
use App\Pemesanan;
use App\Tipe;

class KamarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kamar = Kamar::all();
        return view('Kamar.index', compact('kamar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tipe_id)
    {
        $tipe = Tipe::find($tipe_id);
         
        return view('Kamar.create', compact('tipe')); //Kamar
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nomor_kamar' => 'required',
            'tipe_id' => 'required'
    	]);
 
        kamar::create([
    		'nomor_kamar' => $request->nomor_kamar,
    		'dibooking' => 0,
            'tipe_id' => $request->tipe_id
    	]);
 
    	return redirect('/tipe/'. $request->tipe_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_kamar)
    {
        $pemesanan = Pemesanan::where('kamar_id', $id_kamar)->first();

        $date = date_create($pemesanan->created_at);
        date_add($date,date_interval_create_from_date_string($pemesanan->lama_menginap . " days"));
        $tanggal_berakhir = date_format($date,"Y-m-d");

        return view('admin.showPemesanan', compact('pemesanan', 'tanggal_berakhir'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_kamar)
    {
        $tipe = DB::table('tipe')->get();
        $kamar = Kamar::find($id_kamar);
        return view('Kamar.edit', compact('kamar','tipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
    		'nomor_kamar' => 'required',
    		'dibooking' => 'required',
            'tipe_id' => 'required'
        ]);

        $kamar = Kamar::find($id);
        $kamar->nomor_kamar = $request->nomor_kamar;
        $kamar->dibooking = $request->dibooking;
        $kamar->tipe_id = $request->tipe_id;
        $kamar->update();
        return redirect('/kamar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_kamar)
    {
        $kamar = Kamar::find($id_kamar);
        Kamar::where('id', $id_kamar)->update([
            'dibooking' => 0
        ]);
        $kamar->delete();
        return redirect('/tipe/'.$kamar->tipe->id);
    }
}
