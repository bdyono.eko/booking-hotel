<?php

namespace App\Http\Controllers;

use App\Kamar;
use App\pelanggan;
use App\Pemesanan;
use App\Tipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PelangganController extends Controller
{
    public function Home(){ //halaman 1
        $tipe = Tipe::all();

        return view('users.Home', compact('tipe'));
    } 

    public function pesan($id){ // halaman 2
        $tipe = Tipe::find($id);
        $kosong = Kamar::where('tipe_id', $id)->where('dibooking', 0)->first();

        return view('users.pesan', compact('tipe', 'kosong'));
        // return view('users.pesan');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama' => 'required',
    		'nik' => 'required|max:16|min:16',
            'no_hp' => 'required|max:15',
            'lama_menginap' => 'required'
    	]);

        $pelanggan = pelanggan::create([
            'nik' => $request->nik,
            'nama' => $request->nama,
            'no_hp' => $request->no_hp
        ]);

        $kamar = DB::table('kamar')->where([
            ['tipe_id', $request->tipe_id],
            ['dibooking', 0],
        ])->first();

        $pemesanan = Pemesanan::create([
            'pelanggan_id' => $pelanggan->id,
            'kamar_id' => $kamar->id,
            'lama_menginap' => $request->lama_menginap,
            'total_harga' => $request->lama_menginap * $request->harga,
        ]);

        Kamar::where('id', $kamar->id)
          ->update(['dibooking' => 1]);

    	return redirect('/cekin/'. $pemesanan->id);
    }

    public function cekin($id){ // halaman 3
        $pemesanan = Pemesanan::find($id);
        return view('users.detail', compact('pemesanan'));
    }
}
