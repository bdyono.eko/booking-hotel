<?php

namespace App\Http\Controllers;

use App\Pembayaran;
use App\Pemesanan;
use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    public function test($id){
        $pembayaran = Pembayaran::find($id);

        $pemesanan = Pemesanan::find($pembayaran->pemesanan_id);
        $date = date_create($pemesanan->created_at);
        date_add($date,date_interval_create_from_date_string($pemesanan->lama_menginap . " days"));
        $tanggal_berakhir = date_format($date,"Y-m-d");

        $pdf = PDF::loadView('pdf.test', compact('pembayaran', 'tanggal_berakhir'));
        return $pdf->download('Transaksi-Booking-Hotel-Nusantara.pdf');
    }
}
