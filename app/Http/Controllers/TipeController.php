<?php

namespace App\Http\Controllers;

use App\Kamar;
use App\Pemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Tipe;

class TipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipe = Tipe::all();
        return view('tipe.index', compact('tipe'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipe.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama_tipe' => 'required',
    		'harga' => 'required',
            'deskripsi' => 'required',
            'fasilitas' => 'required',
            'gambar' => 'mimes: jpg,png,jpeg|max:2200'
    	]);

        $gambar = $request->gambar;
        $new_gambar = uniqid('img-') . '.jpg';
 
        Tipe::create([
    		'nama_tipe' => $request->nama_tipe,
    		'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'fasilitas' => $request->fasilitas,
            'gambar' => $new_gambar,
    	]);
        $gambar->move('images/', $new_gambar);
 
    	return redirect('/tipe');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tipe_id)
    {
        $tipe = Tipe::find($tipe_id);
        $kamar = Kamar::where('tipe_id', $tipe_id)->get();

        return view('tipe.show', compact('tipe', 'kamar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tipe_id)
    {
        $tipe = Tipe::find($tipe_id);
        return view('tipe.edit', compact('tipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tipe_id)
    {
        $this->validate($request,[
    		'nama_tipe' => 'required',
    		'harga' => 'required',
            'deskripsi' => 'required',
            'fasilitas' => 'required',
            'gambar' => 'mimes: jpg,png,jpeg|max:2200'
    	]);
        $tipe = Tipe::findorfail($tipe_id);

        if ($request->has('gambar')){
            $path = "images/";
            File::delete($path.$tipe->gambar);

            $gambar = $request->gambar;
            $new_gambar = uniqid('img-') . '.jpg';
            $gambar->move('images/', $new_gambar);

            $gambar_data = [
            'nama_tipe' => $request->nama_tipe,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'fasilitas' => $request->fasilitas,
            'gambar' => $new_gambar
            ];
        } else {
            $gambar_data = [
            'nama_tipe' => $request->nama_tipe,
            'harga' => $request->harga,
            'deskripsi' => $request->deskripsi,
            'fasilitas' => $request->fasilitas
            ];
        }
        $tipe->update($gambar_data);
        return redirect('/tipe');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tipe_id)
    {
        $tipe = Tipe::findorfail($tipe_id);
        $tipe->delete();

        $path = 'images/';
        File::delete($path.$tipe['gambar']);
        return redirect('/tipe');
    }
}
