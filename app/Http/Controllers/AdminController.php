<?php

namespace App\Http\Controllers;

use App\Kamar;
use App\Pembayaran;
use App\Pemesanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{
    public function home(){
        $pembayaran = Pembayaran::where('konfirmasi', 0)->get();
        return view('admin.home', compact('pembayaran'));
    }

    public function showPemesanan($id){
        $pemesanan = Pemesanan::find($id);

        $date = date_create($pemesanan->created_at);
        date_add($date,date_interval_create_from_date_string($pemesanan->lama_menginap . " days"));
        $tanggal_berakhir = date_format($date,"Y-m-d");

        return view('admin.showPemesanan', compact('pemesanan', 'tanggal_berakhir'));
    }

    public function editPemesanan($id){
        Pembayaran::where('pemesanan_id', $id)->update([
            'konfirmasi' => 1,
            'user_id' => Auth::user()->id
        ]);

        return redirect('/terkonfirmasi');
    }

    public function batalPemesanan($id){
        Pembayaran::where('pemesanan_id', $id)->update([
            'konfirmasi' => 0,
        ]);

        return redirect('/belumDikonfirmasi');
    }

    public function terkonfirmasi(){
        $pembayaran = Pembayaran::where('konfirmasi', 1)->get();

        return view('admin.terkonfirmasi', compact('pembayaran'));
    }

    public function destroy(Request $request, $id)
    {
        $pemesanan = Pemesanan::findorfail($id);

        Kamar::where('id', $pemesanan->kamar_id)->update([
            'dibooking' => 0,
        ]);

        $path = 'images/bukti_bayar/';
        File::delete($path . $pemesanan->pembayaran->bukti_bayar);

        $alamat = $request->alamat;

        $pemesanan->delete();

        return redirect('/'.$alamat);
    }
}
