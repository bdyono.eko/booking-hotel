<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pelanggan extends Model
{
    protected $table = "pelanggan";
    protected $fillable = ["nik", "nama", "no_hp", 'created_at', 'updated_at'];
    public $timestamps = true;

    public function pemesanan(){
        return $this->hasMany('App\Pemesanan');
    }
}
