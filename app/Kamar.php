<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kamar extends Model
{
    protected $table = "kamar";
    protected $fillable = ["nomor_kamar", "dibooking", "tipe_id", "created_at", "updated_at"];
    public $timestamps = true;

    public function tipe(){
        return $this->belongsTo('App\Tipe');
    }

    public function pemesanan(){
        return $this->hasMany('App\Pemesanan');
    }
}
