<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/admin">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-hotel"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Hotel Nusantara</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider" />

  <!-- Heading -->
  <div class="sidebar-heading">Booking</div>

  <!-- Nav Item - Tables -->
  <li class="nav-item">
    <a class="nav-link" href="/belumDikonfirmasi">
      <i class="fas fa-fw fa-phone-square-alt"></i>
      <span>Belum Terkonfirmasi</span></a
    >
  </li>

  <!-- Nav Item - Tables -->
  <li class="nav-item">
    <a class="nav-link" href="/terkonfirmasi">
      <i class="fas fa-fw fa-phone-square-alt"></i>
      <span>Terkonfirmasi</span></a
    >
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider" />

  <!-- Heading -->
  <div class="sidebar-heading">Hotel</div>

  <!-- Nav Item - Tables -->
  <li class="nav-item">
    <a class="nav-link" href="/tipe">
      <i class="fas fa-fw fa-bed"></i>
      <span>Tipe Kamar</span></a
    >
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block" />

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->
