@extends('templates.user')

@section('content')
<h4 class="text-center my-4">Tipe Kamar</h4>

<div class="row justify-content-center p-3">
  @foreach ($tipe as $t)
  <div class="col-md-4 mb-3">
    <div class="card shadow">
      <img class="card-img-top" src="images/{{ $t->gambar }}" alt="Card image cap" style="height: 250px; width: 100%">
      <div class="card-body">
        <h5 class="card-title">{{ $t->nama_tipe }}</h5>
        <p class="card-text">Rp. {{ $t->harga }} /hari</p>
        <a href="/pesan/{{ $t->id }}" class="btn btn-primary">Booking</a>
      </div>
    </div>
  </div>
  @endforeach
</div>
@endsection