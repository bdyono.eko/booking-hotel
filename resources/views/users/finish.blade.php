@extends('templates.user')
   
@section('content')
    <div class="container"> 
        <div class="row text-dark" >
            <div class="col-md-6 my-3"> <!--Kiri-->
                <h3 class="fw-bold mb-4">Transaksi Selesai</h3>
                <div class="row">
                    <div class="col-4">NIK</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pembayaran->pemesanan->pelanggan->nik }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Nama</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pembayaran->pemesanan->pelanggan->nama }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Tipe</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pembayaran->pemesanan->kamar->tipe->nama_tipe }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Harga</div>
                    <div class="col-1">:</div>
                    <div class="col-7">Rp. {{ $pembayaran->pemesanan->kamar->tipe->harga }} /Hari</div>
                </div>
                <div class="row">
                    <div class="col-4">Lama Menginap</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pembayaran->pemesanan->lama_menginap }} Hari</div>
                </div>
                <div class="row">
                    <div class="col-4">Total Pembayaran</div>
                    <div class="col-1">:</div>
                    <div class="col-7">Rp. {{ $pembayaran->pemesanan->total_harga }}</div>
                </div>
            </div>   
            <div class="col-6 my-3"> <!--Kanan-->
              <img src="{{asset('images/bukti_bayar/'. $pembayaran->bukti_bayar)}}" alt="" style="height: 250px; width: 100%">
            </div>   
        </div>
        <div class="mt-3">
          <a href="/test/{{$pembayaran->id}}" class="btn btn-warning">Download Bukti Transaksi <i class="fas fa-file-pdf text-danger"></i></a>
          <a href="/" class="btn btn-primary">Back to Home</a>
        </div>
    </div>
@endsection