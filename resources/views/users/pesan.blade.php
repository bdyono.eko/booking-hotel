@extends('templates.user')
    
@section('content')
        <div class="row">
            <div class="col-md-6 mb-3"> <!--Kiri-->
                <div class="border bg-light p-4">
                    <img class="card-img-top" src="{{asset('images/' . $tipe->gambar)}}" alt="Card image cap" style="height: 250px; width: 100%">
                    <h4 class="display-6 text-dark mt-3"><b>Deskripsi</b></h4>
                    <p class="text-dark" ><?= $tipe->deskripsi; ?></p>
                    <hr class="my-4">
                    <p>Fasilitas : <b>{{$tipe->fasilitas}}</b></p>
                    <p class="lead">
                        <a class="btn btn-success" disabled>Harga  : <b>Rp. {{$tipe->harga}} /hari</b></a>
                    </p>
                </div>
            </div>   

            @if ($kosong != null)
            <div class="col-md-6"> <!--Kanan-->
                <h3 class="mb-3">Form Booking</h3>
                <form action="/cekin" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="text" class="form-control" id="nama" value="{{ old('nama') }}" name="nama" placeholder="Masukan Nama">
                        @error('nama')
                            <div class="alert alert-danger mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="NIK">NIK</label>
                        <input type="number" class="form-control" id="NIK" value="{{ old('nik') }}" name="nik" placeholder="NIK">
                        @error('nik')
                            <div class="alert alert-danger mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="no">No. HP</label>
                        <input type="number" class="form-control" id="no" name="no_hp" value="{{ old('no_hp') }}" placeholder="Nomor telepon">
                        @error('no_hp')
                            <div class="alert alert-danger mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="menginap">Lama Menginap</label>
                        <input type="number" class="form-control" id="lama_menginap" name="lama_menginap" value="{{ old('lama_menginap') }}" placeholder="Masukkan total hari">
                        @error('lama_menginap')
                            <div class="alert alert-danger mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <input type="hidden" name="tipe_id" value="{{ $tipe->id }}">
                    <input type="hidden" name="harga" value="{{ $tipe->harga }}">
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </form>
            </div>   
            @else
            <div class="col-md-6">
                <div class="card border-danger mb-3" style="width: 100%">
                    <div class="card-body text-danger">
                      <h5 class="card-title text-center">Mohon Maaf Kamar Dengan Tipe Ini Penuh</h5>
                    </div>
                </div>
            </div>
            @endif
        </div>
@endsection