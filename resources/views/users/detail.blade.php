@extends('templates.user')
   
@section('content')
    <div class="container"> 
        <div class="row text-dark" >
            <div class="col-md-6 my-3"> <!--Kiri-->
                <h3 class="fw-bold mb-4">Detail Booking</h3>
                <div class="row">
                    <div class="col-4">NIK</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->pelanggan->nik }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Nama</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->pelanggan->nama }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Tipe</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->kamar->tipe->nama_tipe }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Harga</div>
                    <div class="col-1">:</div>
                    <div class="col-7">Rp. {{ $pemesanan->kamar->tipe->harga }} /Hari</div>
                </div>
                <div class="row">
                    <div class="col-4">Lama Menginap</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->lama_menginap }} Hari</div>
                </div>
                <div class="row">
                    <div class="col-4">Total Pembayaran</div>
                    <div class="col-1">:</div>
                    <div class="col-7">Rp. {{ $pemesanan->total_harga }}</div>
                </div>
            </div>   
            <div class="col-6 my-3"> <!--Kanan-->
                <div class="mb-3">
                    <h3 class="fw-bold mb-3">No Rekening</h3>
                    <h4>123 456 789 000</h4>
                </div>
                <form action="/pembayaran" method="post" enctype="multipart/form-data">
                    @csrf
                    <p class="mb-1">Upload Bukti Pembayaran</p>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="bukti_bayar" name="bukti_bayar">
                        <label class="custom-file-label" for="bukti_bayar">Choose file</label>
                        @error('bukti_bayar')
                            <div class="alert alert-danger mt-1 mb-3">{{ $message }}</div>
                        @enderror
                    </div>
                    <input type="hidden" name="pemesanan_id" value="{{ $pemesanan->id}}">
                    <button type="submit" class="btn btn-primary mt-3">Upload</button>
                </form>
            </div>   
        </div>
    </div>
@endsection