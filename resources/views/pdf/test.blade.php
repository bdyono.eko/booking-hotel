<html>
    <head>
        <title>Transaksi</title>
    </head>
    <body>
        <div>
            <h1>Detail Transaksi <?= $pembayaran->pemesanan->pelanggan->nama; ?></h1>
            <table border="1">
                <tr>
                    <td>NIK</td>
                    <td>:</td>
                    <td><?= $pembayaran->pemesanan->pelanggan->nik; ?></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><?= $pembayaran->pemesanan->pelanggan->nama; ?></td>
                </tr>
                <tr>
                    <td>Tipe</td>
                    <td>:</td>
                    <td><?= $pembayaran->pemesanan->kamar->tipe->nama_tipe; ?></td>
                </tr>
                <tr>
                    <td>Harga</td>
                    <td>:</td>
                    <td>Rp. <?= $pembayaran->pemesanan->kamar->tipe->harga; ?>,00 </td>
                </tr>
                <tr>
                    <td>Lama Menginap</td>
                    <td>:</td>
                    <td><?= $pembayaran->pemesanan->lama_menginap; ?> Hari</td>
                </tr>
                <tr>
                    <td>Tanggal Booking</td>
                    <td>:</td>
                    <td><?= $pembayaran->pemesanan->created_at; ?></td>
                </tr>
                <tr>
                    <td>Tanggal Berakhir</td>
                    <td>:</td>
                    <td><?= $tanggal_berakhir; ?></td>
                </tr>
                <tr>
                    <td>Total Pembayaran</td>
                    <td>:</td>
                    <td><?= $pembayaran->pemesanan->total_harga; ?></td>
                </tr>
            </table>
        </div>
    </body>
</html>