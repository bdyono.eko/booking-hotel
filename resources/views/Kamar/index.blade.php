@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css"/>
@endpush

@extends('templates.master')

@section('judul1')
  List Kamar
@endsection

@section('judul2')
  Daftar kamar
@endsection

@section('content')
<div class="mb-3">
  <a href="/kamar/create" class="btn btn-info">Tambah <small><i class="fas fa-plus"></i></small></a>
</div>

<table class="table table-hover" id="dataTable">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">nomor kamar</th>
      <th scope="col">Keterangan(Kosong(0)/Penuh(1))</th>
      <th scope="col">tipe kamar(berdasarkan Tipe_id)</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($kamar as $key => $all)
    <tr>
      <th scope="row">{{ $key + 1 }}</th>
      <td>{{ $all->nomor_kamar }}</td>
      <td>{{ $all->dibooking }}</td>
      <td>{{ $all->tipe_id }}</td>
      <td style="display: flex;">
          <a href="/kamar/{{$all->id}}/edit" class="btn btn-info btn-sm mr-2">Edit</a>
          <form action="/kamar/{{$all->id}}" method="post">
          @csrf
          @method('DELETE')
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
          </form>
      </td>
    </tr>
    @empty
      No Data
    @endforelse
  </tbody>
</table>
@endsection

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.js"></script>
<script src="{{ asset('/template/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/template/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#dataTable").DataTable();
  });
</script>
@endpush