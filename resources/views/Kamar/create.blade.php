@extends('templates.master')

@section('judul1')
 Tipe Kamar
@endsection

@section('judul2')
Membuat Ruang Kamar
@endsection
    
@section('content')
  <!-- form start -->
  <form role="form" action="/kamar" method="POST">
    @csrf
    <div class="card-body">
      <div class="form-group">
        <label for="nomor_kamar">Nomor Kamar</label>
        <input type="text" class="form-control" id="nomor_kamar" name="nomor_kamar" placeholder="Nomor kamar">
        @error('nomor_kamar')
          <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="tipe_kamar">Tipe kamar</label>
        <input type="text" class="form-control" value="{{$tipe->nama_tipe}}" id="tipe_kamar" readonly>
        <input type="hidden" value="{{$tipe->id}}" name="tipe_id">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection

