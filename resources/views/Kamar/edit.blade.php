@extends('templates.master')

@section('judul1')
 Tipe Kamar
@endsection

@section('judul2')
 Cek list kamar kosong
@endsection
    
@section('content')
  <!-- form start -->
  <form role="form"  action="/kamar/{{$kamar->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="card-body">
      <div class="form-group">
        <label for="nomor_kamar">Nomor Kamar</label>
        <input type="text" class="form-control" id="nomor_kamar" value="{{$kamar->nomor_kamar}}" name="nomor_kamar" placeholder="Nomor kamar">
        @error('nomor_kamar')
          <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="dibooking">Keterangan</label>
        <select name="dibooking" id="dibooking" class="form-control">
          <option value="{{ $ }}"></option>
          <option value="" disabled> --isi keterangan-- </option>
          <option value="1"> Penuh</option>
          <option value="0"> Kosong</option>
        </select>
        @error('dibooking')
          <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="tipe_id">Tipe kamar</label>
        <select name="tipe_id" id="tipe_id" class="form-control">
          <option value="" disabled> --pilih tipe kamar-- </option>
          <option value="{{$kamar->id}}"> {{$kamar->nama_tipe}}</option>
          @foreach ($tipe as $item)
            <option value="{{$item->id}}"> {{$item->nama_tipe}}</option>
          @endforeach
        </select>
        @error('tipe_id')
          <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection

