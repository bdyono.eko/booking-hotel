@extends('templates.user')

@section('content')
            <div class="row justify-content-center">
              <div class="col-md-5">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Login Admin</h1>
                  </div>
                  <form class="user" action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user @error('email') is-invalid @enderror" value="{{ old('email') }}" id="email" name="email" aria-describedby="emailHelp" placeholder="Email" required />
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" id="password" name="password" placeholder="Password" required />
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block"> Login </button>
                  </form>
                </div>
              </div>
            </div>
          
@endsection
