@extends('templates.master')

@section('judul1')
  Detail Tipe Kamar
@endsection

@section('judul2')
{{$tipe->nama_tipe}}
@endsection

@section('content')
<div class="row">
  <div class="col-md-6 mb-3"> <!--Kiri-->
      <div class="border bg-light p-4">
          <img class="card-img-top" src="{{asset('images/' . $tipe->gambar)}}" alt="Card image cap" style="height: 250px; width: 100%">
          <h4 class="display-6 text-dark mt-3"><b>Deskripsi</b></h4>
          <p class="text-dark" ><?= $tipe->deskripsi ?></p>
          <hr class="my-4">
          <p>Fasilitas : <b>{{$tipe->fasilitas}}</b></p>
          <p class="lead">
          <p>Harga  : <b>Rp. {{$tipe->harga}}</b></p>
          </p>
      </div>
      <a href="/tipe" class="btn btn-info mt-3">Back</a>
  </div>   
  <div class="col-md-6">
    <div class="mb-3">
      <form action="/kamar/create/{{$tipe->id}}" method="post">
        @csrf
        <button type="submit" class="btn btn-info">Tambah <small><i class="fas fa-plus"></i></small></button>
      </form>
    </div>
    
    <h3>Ruang Kamar</h3>

    <div class="row justify-content-center">

      @foreach ($kamar as $k)
      @if ($k->dibooking == 0)
      <div class="col-md-4">
        <div class="card border-info mb-3" style="width: 100%">
          <div class="card-header">{{$k->nomor_kamar}}</div>
          <div class="card-body text-info">
            <h5 class="card-title">Kosong</h5>
          </div>
          <div class="card-footer" style="display: flex;">

            <form action="/kamar/{{$k->id}}" method="post">
            @csrf
            @method('DELETE')
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>

          </div>
        </div>
      </div>
      @else
      <div class="col-md-4">
        <div class="card border-danger mb-3" style="width: 100%">
          <div class="card-header">{{$k->nomor_kamar}}</div>
          <div class="card-body text-danger">
            <h5 class="card-title">Penuh</h5>
          </div>
          <div class="card-footer" style="display: flex;">
            <a href="/kamar/{{$k->id}}" class="btn btn-info btn-sm mr-1">Detail</a>
            <form action="/kamar/{{$k->id}}" method="post">
            @csrf
            @method('DELETE')
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>

          </div>
        </div>
      </div>
      @endif
      @endforeach

    </div>
  </div> 
</div>
@endsection