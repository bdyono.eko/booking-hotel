@extends('templates.master')

@section('judul1')
 Tipe Kamar
@endsection

@section('judul2')
 Buat Tipe Kamar
@endsection
    
@section('content')
  <!-- form start -->
  <form role="form" action="/tipe" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
      <div class="form-group">
        <label for="nama_tipe">Nama Tipe Kamar</label>
        <input type="text" class="form-control" id="nama_tipe" value="{{ old('nama_tipe') }}" name="nama_tipe" placeholder="Nama Tipe">
        @error('nama_tipe')
          <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="harga">Harga</label>
        <input type="number" class="form-control" id="harga" name="harga" value="{{ old('harga_kamar') }}" placeholder="Harga Kamar">
        @error('harga')
          <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="deskripsi">Deskripsi</label>
        <textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi Kamar">{{ old('deskripsi') }}</textarea>
        @error('deskripsi')
          <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="fasilitas">Fasilitas</label>
        <input type="text" class="form-control" id="fasilitas" value="{{ old('fasilitas') }}" name="fasilitas" placeholder="Fasilitas Kamar">
        @error('harga')
          <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
      </div>
      {{-- <div class="custom-file">
        <input type="file" class="custom-file-input" id="customFile">
        <label class="custom-file-label" for="customFile">Choose file</label>
      </div> --}}

      <div class="form-group">
        <label for="gambar">Gambar</label>
        <div class="custom-file">
          <label for="gambar" class="custom-file-label">Choose file</label>
          <input type="file" class="custom-file-input" id="gambar" name="gambar">
          @error('gambar')
            <div class="alert alert-danger mt-1">{{ $message }}</div>
          @enderror
        </div>
      </div>

      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
@endsection

@push('scripts')
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    tinymce.init({
      selector: '#deskripsi'
    });
  </script>
@endpush

