@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css"/>
@endpush

@extends('templates.master')

@section('judul1')
  Tipe Kamar
@endsection

@section('judul2')
  Daftar Tipe Kamar
@endsection

@section('content')
<div class="mb-3">
  <a href="/tipe/create" class="btn btn-info">Tambah <small><i class="fas fa-plus"></i></small></a>
</div>

<table class="table table-hover" id="dataTable">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Tipe</th>
      <th scope="col">Harga</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($tipe as $key => $t)
    <tr>
      <th scope="row">{{ $key + 1 }}</th>
      <td>{{ $t->nama_tipe }}</td>
      <td>{{ $t->harga }}</td>
      <td style="display: flex;">
          <a href="/tipe/{{$t->id}}" class="btn btn-warning btn-sm mr-2">Detail</a>
          <a href="/tipe/{{$t->id}}/edit" class="btn btn-info btn-sm mr-2">Edit</a>
          <form action="/tipe/{{$t->id}}" method="post">
          @csrf
          @method('DELETE')
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
          </form>
      </td>
    </tr>
    @empty
      No Data
    @endforelse
  </tbody>
</table>
@endsection

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.js"></script>
<script src="{{ asset('/template/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/template/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#dataTable").DataTable();
  });
</script>
@endpush