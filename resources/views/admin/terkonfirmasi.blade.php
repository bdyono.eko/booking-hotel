@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css"/>
@endpush

@extends('templates.master')

@section('judul1')
  Pemesanan
@endsection

@section('judul2')
  Terkonfirmasi
@endsection
    
@section('content')
<table class="table table-hover" id="dataTable">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Tipe Kamar</th>
      <th scope="col">Nomor Kamar</th>
      <th scope="col">Tanggal Booking</th>
      <th scope="col">Lama Menginap</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($pembayaran as $key => $p)
    <tr>
      <th scope="row">{{ $key + 1 }}</th>
      <td>{{ $p->pemesanan->pelanggan->nama }}</td>
      <td>{{ $p->pemesanan->kamar->tipe->nama_tipe }}</td>
      <td>{{ $p->pemesanan->kamar->nomor_kamar }}</td>
      <td>{{ $p->pemesanan->created_at }}</td>
      <td>{{ $p->pemesanan->lama_menginap }} Hari</td>
      <td style="display: flex;">
          <a href="/pemesanan/{{$p->pemesanan_id}}" class="btn btn-info btn-sm mr-2">Detail</a>
          <form action="/batalPemesanan/{{$p->pemesanan_id}}/edit" method="post">
            @csrf
            @method('put')
            <button type="submit" class="btn btn-warning btn-sm mr-2">Batal konfirmasi</button>
          </form>
          <form action="/pemesanan/{{$p->pemesanan_id}}" method="post">
          @csrf
          @method('DELETE')
            <input type="hidden" name="alamat" value="terkonfirmasi">
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
          </form>
      </td>
    </tr>
    @empty
      No Data
    @endforelse
  </tbody>
</table>
@endsection

@push('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.js"></script>
<script src="{{ asset('/template/vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/template/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#dataTable").DataTable();
  });
</script>
@endpush