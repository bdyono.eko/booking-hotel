@extends('templates.master')

@section('judul1')
    Pemesanan
@endsection

@section('judul2')
    Detail Transaksi
@endsection
   
@section('content')
    <div class="container"> 
        <div class="row text-dark" >
            <div class="col-md-6 my-3"> <!--Kiri-->
                <h3 class="fw-bold mb-4">Transaksi Selesai</h3>
                <div class="row">
                    <div class="col-4">NIK</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->pelanggan->nik }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Nama</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->pelanggan->nama }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Tipe</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->kamar->tipe->nama_tipe }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Nomor Kamar</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->kamar->nomor_kamar }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Harga</div>
                    <div class="col-1">:</div>
                    <div class="col-7">Rp. {{ $pemesanan->kamar->tipe->harga }} /Hari</div>
                </div>
                <div class="row">
                    <div class="col-4">Tanggal Booking</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->created_at }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Lama Menginap</div>
                    <div class="col-1">:</div>
                    <div class="col-7">{{ $pemesanan->lama_menginap }} Hari</div>
                </div>
                <div class="row">
                  <div class="col-4">Tanggal Berakhir</div>
                  <div class="col-1">:</div>
                  <div class="col-7">{{ $tanggal_berakhir }}</div>
                </div>
                <div class="row">
                    <div class="col-4">Total Pembayaran</div>
                    <div class="col-1">:</div>
                    <div class="col-7">Rp. {{ $pemesanan->total_harga }}</div>
                </div>
                @if ($pemesanan->pembayaran->konfirmasi == 1)
                    <div class="row">
                        <div class="col-4">Pengkonfirmasi</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $pemesanan->pembayaran->user->name }}</div>
                    </div>
                @endif
            </div>   
            <div class="col-6 my-3"> <!--Kanan-->
              <img src="{{asset('images/bukti_bayar/'. $pemesanan->pembayaran->bukti_bayar)}}" alt="" style="height: 250px; width: 100%">
            </div>   
        </div>
        {{-- <a href="/belumDikonfirmasi" class="btn btn-info mt-3">Back</a> --}}
    </div>
@endsection