<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//user route
Route::group(['middleware' => ['guest']], function(){
  Route::get('/', 'PelangganController@Home');              //halaman 1
  Route::get('/pesan/{id}', 'PelangganController@pesan');    //halaman 2
  Route::get('/cekin/{id}', 'PelangganController@cekin');         //halaman 3
  Route::Post('/cekin', 'PelangganController@store');
  Route::get('/test/{id}', 'PdfController@test');      //test DOMPDF
  Route::resource('/pembayaran', 'PembayaranController');  
});

Route::group(['middleware' => ['auth']], function(){
  //admin route
  Route::get('/belumDikonfirmasi', 'AdminController@home');
  Route::resource('/tipe', 'TipeController'); //CRUD tipe kamar
  Route::post('/kamar/create/{id}', 'KamarController@create');
  Route::resource('/kamar', 'KamarController');  //CRUD kamar
  Route::get('/pemesanan/{id}', 'AdminController@showPemesanan');
  Route::put('/pemesanan/{id}/edit', 'AdminController@editPemesanan');
  Route::put('/batalPemesanan/{id}/edit', 'AdminController@batalPemesanan');
  Route::get('/terkonfirmasi', 'AdminController@terkonfirmasi');
  Route::delete('/pemesanan/{id}', 'AdminController@destroy');
});

// auth route
Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
