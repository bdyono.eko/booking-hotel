<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Moh Adi Ikfini',
                'email' => 'adi.ikfini@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'name' => 'Eko Budiyono',
                'email' => 'bdyono.eko@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'name' => 'Ichtiar Setya Utama',
                'email' => 'isangateruz@gmail.com',
                'password' => Hash::make('12345678'),
            ],
        ]);
    }
}
